package com.example.viktorkachalov.retrofit2reddit.rest;

import com.example.viktorkachalov.retrofit2reddit.model.Feed;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by viktorkachalov on 17.08.17.
 */

//для работы с ретрофитом сперва делаем интерфейс
public interface RedditAPI {

    String Base_URL = "https://www.reddit.com/";
    String Login_URL = "https://www.reddit.com/api/login/";
    
    //GET - то что будем получать в виде json с сайта

    @Headers("Content-Type: application/json")
    @GET(".json")
    Call<Feed> getData();

    //POST - то что будем отправлять на сайт.
    // Разбиваем строчку
    // https://www.reddit.com/api/login/username?user=username&password=myPassword&api-type=json
    // на параметры

    @POST("{user}")
    Call<ResponseBody> login (
            @HeaderMap Map<String,String> headers,
            @Path("user") String username, //username в адресной строке
            @Query("user") String user, // ?user=username
            @Query("passwd") String password, //&passwd=myPassword
            @Query("api-type") String type //&api-type=json
    );


}

